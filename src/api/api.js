import * as axios from 'axios';

function getDataCar(path){
    return new Promise((resolve, reject) => {
      var config = {
          method: 'get',
          url: path,
          headers: {}
        };
        axios(config).then(function (response) {
          resolve(response);
        }).catch(function (error) {
          reject(error);
      });
    })
  }

  export { getDataCar }