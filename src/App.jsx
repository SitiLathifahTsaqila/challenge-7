import Home from "./pages/Home";
import Cars from "./pages/Cars";
import './assets/style.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';


function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component= {Home}></Route>
        <Route path="/cars" component ={Cars}></Route>
      </Switch>
    </Router>
  );
}

export default App;
