import React from 'react'
import Navbar from '../components/Navbar'
import MainSection from '../components/MainSection'
import OurService from '../components/OurService'
import WhyUs from '../components/WhyUs'
import Testimonial from '../components/Testimonial'
import TryNow from '../components/TryNow'
import FAQ from '../components/FAQ'
import Footer from '../components/Footer'

export default function Home() {
  return (
    <>
    <Navbar />
      <MainSection />
        <OurService />
        <WhyUs />
        <Testimonial />
        <TryNow />
        <FAQ />
    <Footer />
    </>
  )
}
