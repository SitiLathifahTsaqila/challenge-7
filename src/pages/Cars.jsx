import React from 'react'
import Navbar from '../components/Navbar'
import SearchBox from '../components/SearchBox'
import MainSection from '../components/MainSection'
import Footer from '../components/Footer'
import CarData from '../components/CarData'


export default function Cars() {
  return (
    <>
    <Navbar />
    <MainSection />
    <SearchBox />
    <CarData />
    <Footer />
    </>
  )
}
