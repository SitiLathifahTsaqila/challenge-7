import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'


const trainingState = {
  car: []
}
const trainingReducer = (state = trainingState, action) =>{
  if(action.type === "get_car"){
      return {
          ...trainingState,
          car:  [...state.car, ...action.payload]
      }
  }
  if(action.type === "filter_car"){
    state.car = [];
    return {
      ...trainingState,
      car:  [...state.car, ...action.payload]
    }
  }
  return state;
}

const storeRedux = createStore(trainingReducer, applyMiddleware(thunk))
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Provider store={storeRedux}><App /></Provider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
