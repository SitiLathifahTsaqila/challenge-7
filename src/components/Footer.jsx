import React from 'react'
import { Container, Row, Col} from 'react-bootstrap'
import facebook from '../assets/image/icon_facebook.png'
import instagram from '../assets/image/icon_instagram.png'
import twitter from '../assets/image/icon_twitter.png'
import mail from '../assets/image/icon_mail.png'
import twitch from '../assets/image/icon_twitch.png'

export default function Footer() {
  return (
    <footer className="text-lg-start">
    
    <Container className="p-4">
     
      <Row>
       
        <Col lg= {3} md={6} className ="mb-4 mb-md-0">
          <ul className="list-unstyled mb-0">
            <li>
              <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
            </li>
            <li>
              <p>binarcarrental@gmail.com</p>
            </li>
            <li>
              <p>081-233-334-808</p>
            </li>
          </ul>
        </Col>
      

       
        <Col lg={2} md={6} className="mb-4 mb-md-0">
          <ul className="list-unstyled">
              <li className="mb-3">
                  <a href="#toOurService" className="text-dark text-decoration-none">Our Services</a>
                </li>
                <li className="mb-3">
                  <a href="#toWhyUs" className="text-dark text-decoration-none">Why Us</a>
                </li>
                <li className="mb-3">
                  <a href="#toTestimonial" className="text-dark text-decoration-none">Testimonial</a>
                </li>
                <li>
                  <a href="#faq" className="text-dark text-decoration-none">FAQ</a>
                </li>
          </ul>
        </Col>
    
        <Col lg={4} md={6} className="mb-4 mb-md-0">
          <ul className="list-unstyled">
              <li>
                  <p>Connect With Us</p>
              </li>
                <li>
                  <img src= {facebook} alt="" className="icon"/>
                  <img src= {instagram} alt="" className="icon" />
                  <img src= {twitter} alt="" className="icon" />
                  <img src= {mail} alt="" className="icon" />
                  <img src= {twitch} alt="" className="icon" />
                </li>
          </ul>
        </Col>
   
        <Col lg={3} md={6} className="mb-4 mb-md-0">
          <ul className="list-unstyled">
              <li>
                  <p>Copyright Binar 2022</p>
                </li>
                <li>
                  <img src="image/logo.png" alt="" />
                </li>
          </ul>
        </Col>
      </Row>
  </Container>
  </footer>
  )
}
