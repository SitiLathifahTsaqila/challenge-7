import React from 'react'
import OwlCarousel from 'react-owl-carousel';  
import 'owl.carousel/dist/assets/owl.carousel.css';  
import 'owl.carousel/dist/assets/owl.theme.default.css';  
import {Row, Col, Card } from 'react-bootstrap';

import star from '../assets/image/star.png'
import profil1 from '../assets/image/img_photo-1.png'
import profil2 from '../assets/image/img_photo-2.png'

export default function Testimonial() {
  return (
    <section className="testimonial" id="toTestimonial">
    <Row className="text-center mb-4" style={{margin: 0}}>
        <h2 className="title-2">Testimonial</h2>
        <p className="description">Berbagai review positif dari para pelanggan kami</p>
    </Row>

    <OwlCarousel 
    items= {2}
    margin= {20}    
    loop= {true}
    center= {true}
    nav= {true}
    autoplay= {true}
    responsiveClass={true}
    responsive= {{
        0: {
            items: 1,
            nav: true
        },
        576: {
            items: 1,
            nav: true
        },
        768: {
            items: 2,
            nav: true,
        }
        }}>

            <div className="bg-testimonial d-flex align-items-center testi-card">
                <div className="d-flex justify-content-center user-img" >
                    <img src={profil1} alt="Photo" className="img-testimonial" />
                </div>
                <Col md={8}>
                    <Card.Body className="desc-card">
                      <img src= {star} alt="star" className="img-star"  />
                      <p className="testi-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                      sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                      lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                      <p className="testi-user">John Dee 32, Bromo</p>
                    </Card.Body>
                </Col>
            </div>

            <div className="bg-testimonial d-flex align-items-center testi-card">
                <div className="d-flex justify-content-center user-img" >
                    <img src={profil2} alt="Photo" className="img-testimonial" />
                </div>
                <Col md ={8}>
                    <Card.Body className="desc-card">
                      <img src={star} alt="star" className="img-star" />
                      <p className="testi-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                      sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                      lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                      <p className="testi-user">John Dee 32, Bromo</p>
                    </Card.Body>
                </Col>
            </div>
            <div className="bg-testimonial d-flex align-items-center testi-card">
                <div className="d-flex justify-content-center user-img" >
                    <img src={profil1} alt="Photo" className="img-testimonial" />
                </div>
                <Col md={8}>
                    <Card.Body className="desc-card">
                      <img src={star} alt="star" className="img-star" />
                      <p className="testi-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                      sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                      lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                      <p className="testi-user">John Dee 32, Bromo</p>
                    </Card.Body>
                </Col>
            </div>

            <div className="bg-testimonial d-flex align-items-center testi-card">
                <div className="d-flex justify-content-center user-img" >
                    <img src={profil2} alt="Photo" className="img-testimonial" style={{width: '45%', height: '45%'}}/>
                </div>
                <Col md={8}>
                    <Card.Body className="desc-card">
                      <img src= {star} alt="star" className="img-star"/>
                      <p className="testi-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                      sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                      lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                      <p className="testi-user">John Dee 32, Bromo</p>
                    </Card.Body>
                </Col>
            </div>

            <div className="bg-testimonial d-flex align-items-center testi-card">
                <div className="d-flex justify-content-center user-img" >
                    <img src={profil1} alt="Photo" className="img-testimonial" />
                </div>
                <Col md={8}>
                    <Card.Body className="desc-card">
                      <img src={star} alt="star" className="img-star" />
                      <p className="testi-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                      sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                      lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                      <p className="testi-user">John Dee 32, Bromo</p>
                    </Card.Body>
                </Col>
            </div>

            <div className="bg-testimonial d-flex align-items-center testi-card">
                <div className="d-flex justify-content-center user-img" >
                    <img src={profil2} alt="Photo" className="img-testimonial" style={{width: '45%', height: '45%'}}/>
                </div>
                <Col md={8}>
                    <Card.Body className="desc-card">
                      <img src= {star} alt="star" className="img-star"/>
                      <p className="testi-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                      sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                      lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                      <p className="testi-user">John Dee 32, Bromo</p>
                    </Card.Body>
                </Col>
            </div>
      </OwlCarousel> 
</section>
  )
}
