import React from 'react'
import {Container, Button} from 'react-bootstrap'

export default function TryNow() {
  return (
    <Container id="TryNow">
    <div className="py-4 text-center my-4 rounded-3" style={{backgroundColor: '#0D28A6'}}>
        <h1 className="text-white">Sewa Mobil di (Lokasimu) Sekarang</h1>
        <p className="text-white">Lorem ipsum dolor sit amet, consectetur 
        adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
        aliqua.<br /> </p>
        <Button type="button" className="btn-success">Mulai sewa mobil</Button>
    </div>
</Container>
  )
}
