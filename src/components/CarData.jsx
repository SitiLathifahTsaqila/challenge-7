import { connect } from 'react-redux';
import { getDataCar } from '../api/api';
import React from 'react';
import user from '../assets/image/fi_users.png'
import setting from '../assets/image/fi_settings.png'
import calendar from '../assets/image/fi_calendar.png'

class CarData extends React.Component{
    constructor(props) {
        super(props);
        // 
    }
    async componentDidMount() {
        if (this.first) return; this.first = true;
        var getCar = await getDataCar("https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json").then(result => result);
        if(getCar.data.length > 0){
            this.props.dispatch({type: 'get_car', payload: getCar.data});
        }

    }
    render() {
        return (
            <div class="container" style={{paddingTop: '50px', paddingBottom: '50px'}}>
            <div className="cars-content row">
                {this.props.car.map(data => 
                    <div className="col-md-4 my-2" key={data.id}>
                        <div className="card">
                            <div className="card-body h-50">
                                <img src={`https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/public/${data.image}`} 
                                style={{width: '100%', height: '17vw', borderRadius:'2px', marginBottom: '30px'}}/>
                                <h6 className="card-title">{data.manufacture} / {data.model}</h6>
                                <h5 style={{fontWeight: 'bold'}}>Rp {data.rentPerDay} / hari</h5>
                                <p>{data.description}</p>
                                <table style={{height: '110px', marginBottom: '20px'}}>
                                    <tbody>
                                            <tr>
                                                <td><img src={user} /></td>
                                                <td class="spesifikasi">{data.capacity} orang</td>
                                            </tr>
                                            <tr>
                                            <td><img src={setting} /></td>
                                            <td class="spesifikasi">{data.transmission}</td>
                                            </tr>
                                        <tr>
                                            <td><img src={calendar} /></td>
                                            <td class="spesifikasi">Tahun {data.year}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button className="btn btn-success w-100">Pilih Mobil</button>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        </div>
        );

    }
}
const mapState = (state) => {
    return  {
        car: state.car
    }
}
export default connect(mapState)(CarData);