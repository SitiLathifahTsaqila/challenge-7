import React from 'react'
import {Row, Col, Container, Form, Button} from 'react-bootstrap'
import { connect } from 'react-redux';
import { getDataCar } from '../api/api';


class SearchBox extends React.Component {
    constructor(props) {
        super(props);
        this.state={"Drivertype":"Dengan Supir","date":"","hours":"","capacity":""}
        this.handleDriverType = this.handleDriverType.bind(this);
        this.handleDate = this.handleDate.bind(this);
        this.handleHours = this.handleHours.bind(this);
        this.handleCapacity = this.handleCapacity.bind(this);
        this.handleFilter = this.handleFilter.bind(this);
    };
    handleDriverType(event){
        this.setState({"Drivertype":event.target.value})
        console.log(this.state.Drivertype)
    }
    handleDate(event){
        this.setState({"date":event.target.value})
        console.log(event.target.value)
    }
    handleHours(event){
        this.setState({"hours":event.target.value})
        console.log(event.target.value)
    }
    handleCapacity(event){
        this.setState({"capacity":event.target.value})
        console.log(event.target.value)
    }
    async handleFilter(){
        var getCar = await getDataCar("https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json").then(result => result);
        var CarFilter = []
        if(getCar.data.length > 0 && this.state.Drivertype!=='' && this.state.date!=='' && this.state.hours!=='' && this.state.capacity!=='' ){
            for(var i = 0; i< getCar.data.length; i++){
                if(getCar.data[i].availableAt.slice(0,10) === this.state.date ){
                    if(getCar.data[i].availableAt.slice(11,16) <= this.state.hours && getCar.data[i].capacity >= this.state.capacity){
                        CarFilter.push(getCar.data[i])
                    }
                }else if(getCar.data[i].availableAt.slice(0,10) <= this.state.date && getCar.data[i].capacity >= this.state.capacity){
                    CarFilter.push(getCar.data[i])
                }
            } 
            this.props.dispatch({type: 'filter_car', payload: CarFilter});
        }else if(this.state.Drivertype===''||this.state.date===''||this.state.hours===''||this.state.capacity===''){
            alert("data not found")
        }
    }
render() {
    return (
        <Container className="filter filter-container">
         <section className="d-flex justify-content-center pt-0">
        <div className="search-box pt-0">
            <div className="d-flex form-search justify-content-center mt-3 ">
                <Row className="row-cols-lg-auto align-items-center">
                
             <Col md={6} className="mb-lg-0">
                <div className="label">Tipe Driver</div> 
                <Form.Select className="form-select-mb-3" aria-label=".form-select-sm example" id="tipeDriver" onChange={this.handleDriverType}> 
                <option selected>Pilih Tipe Driver</option> 
                <option value="1">Dengan Sopir</option> 
                <option value="0">Tanpa Sopir (Lepas Kunci)</option> 
                </Form.Select> 
            </Col> 
 
            <Col md={6} className="mb-lg-0"> 
                <label htmlFor="date" style={{fontSize:"13px"}}>Pilih Tanggal</label><br />
                <input className="form-control" required type="date" name="date" id="date" style={{marginTop: "4px"}}  onChange={this.handleDate} /> 
            </Col> 
 
            <Col md={6} className="mb-lg-0">
                <div className="label">Waktu Jemput/Ambil</div> 
                <Form.Select className="time form-select-sm" aria-label=".form-select-sm example" id="hours"  onChange={this.handleHours}> 
                <option selected>Pilih Waktu</option> 
                <option value="1">08.00 WIB</option> 
                <option value="2">09.00 WIB</option> 
                <option value="3">10.00 WIB</option> 
                <option value="4">11.00 WIB</option> 
                <option value="5">12.00 WIB</option> 
                </Form.Select> 
            </Col> 
 
            <Col md={6} className="mb-lg-0">
                <div className="label">Penumpang (Opsional)</div> 
                <Form.Select className="people form-select-sm" aria-label=".form-select-sm example" id="capacity"  onChange={this.handleCapacity}> 
                <option selected>Jumlah Penumpang</option> 
                <option value="2">1-2</option> 
                <option value="4">3-4</option> 
                <option value="6">5-6</option> 
                </Form.Select> 
            </Col> 
            <Col md ={12}>
                <Button className="btn-search submit" style={{borderColor: "#5cb85f"}} onClick={this.handleFilter}>Cari Mobil</Button>
            </Col>
        </Row> 
               
                
            </div>
        </div>
    </section>
</Container>
  )
    }
}

export default connect()(SearchBox);

