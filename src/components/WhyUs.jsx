import React from 'react'
import {Container, Row, Col, Card} from "react-bootstrap";
import icon_complete from '../assets/image/icon_complete.png'
import icon_price from '../assets/image/icon_price.png'
import icon_24hrs from '../assets/image/icon_24hrs.png'
import icon_profesional from '../assets/image/icon_professional.png'

export default function WhyUs() {
  return (
    <Container id="toWhyUs">
    <h3>Why Us ?</h3>
    <p className="whyus1">Mengapa harus pilih Binar Car Rental?</p>
    <Row className="row-cols-1 row-cols-lg-4 row-cols-md-2 g-3" id="whyus2">
        <Col>
             <Card>
                   <img src={icon_complete} className="card-img-top" alt="..." />
                        <Card.Body>
                            <p className="card-title">Mobil Lengkap</p>
                            <p className="card-text">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat.</p>
                         </Card.Body>
               </Card>
           </ Col>
        <Col>
             <Card>
                   <img src={icon_price} className="card-img-top" alt="..." />
                        <Card.Body>
                            <p className="card-title">Harga Murah</p>
                            <p className="card-text">Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain.</p>
                         </Card.Body>
               </Card>
           </Col>
           <Col>
             <Card>
                   <img src={icon_24hrs} className="card-img-top" alt="..." />
                        <Card.Body>
                            <p className="card-title">Layanan 24 jam</p>
                            <p className="card-text">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu.</p>
                         </Card.Body>
               </Card>
           </Col>
           <Col>
             <Card>
                   <img src={icon_profesional} className="card-img-top" alt="..."  />
                        <Card.Body>
                            <p className="card-title">Supir Porfessional</p>
                            <p className="card-text">Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
                         </Card.Body>
               </Card>
           </Col>
    </Row>
</Container>
  )
}
