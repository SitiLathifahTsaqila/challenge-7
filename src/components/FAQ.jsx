import React from 'react'
import { Accordion, Row, Col, Container } from 'react-bootstrap'

export default function FAQ() {
  return (
	<Container id="FAQ">
		<Row>
			<Col lg={6}>
				<h3 class="fw-bold">Frequently Asked Question</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
           </Col>
			<Col lg={6}>
				<Accordion id="accordionExample">
					<Accordion.Item>
					 <p className="accordion-header" id="headingOne">
					 	<button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">Apa saja syarat yang dibutuhkan?</button>
					 </p>
					 <div id="collapseOne" className="accordion-collapse collapse" aria-labelledby="headingOne">
								<Accordion.Body>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            
							   	</Accordion.Body> 
							</div>
					</Accordion.Item>
				</Accordion>
				<br />
				<Accordion id="accordionExampleTwo">
					<Accordion.Item>
					 <p className="accordion-header" id="headingTwo">
					 	<button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Berapa hari minimal sewa mobil lepas kunci?</button>
					 </p>
							<div id="collapseTwo" className="accordion-collapse collapse" aria-labelledby="headingTwo">
								<Accordion.Body>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            
							   	</Accordion.Body> 
							</div>
					</Accordion.Item>
				</Accordion>
				<br />
				<Accordion id="accordionExampleThree">
					<Accordion.Item>
					 <p className="accordion-header" id="headingThree">
					 	<button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Berapa hari sebelumnya sabaiknya booking sewa mobil?</button>
					 </p>
							<div id="collapseThree" className="accordion-collapse collapse" aria-labelledby="headingThree">
								<Accordion.Body>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            
							   	</Accordion.Body> 
							</div>
					</Accordion.Item>
				</Accordion>
				<br />
				<Accordion id="accordionExampleThree">
					<Accordion.Item>
					 <p className="accordion-header" id="headingFour">
					 	<button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Apakah Ada biaya antar-jemput?</button>
					 </p>
							<div id="collapseFour" className="accordion-collapse collapse" aria-labelledby="headingFour">
								<Accordion.Body>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
							   	</Accordion.Body> 
							</div>
					</Accordion.Item>
				</Accordion>
				<br />
				<Accordion id="accordionExampleFour">
					<Accordion.Item>
					 <p className="accordion-header" id="headingFive">
					 	<button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">Bagaimana jika terjadi kecelakaan?</button>
					 </p>
							<div id="collapseFive" className="accordion-collapse collapse" aria-labelledby="headingFive">
								<Accordion.Body>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            
							   	</Accordion.Body> 
							</div>
					</Accordion.Item>
				</Accordion>
				<br />
			</Col>
		</Row>
	</Container>
  )
}
