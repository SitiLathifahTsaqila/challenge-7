import React from 'react'
import logo from '../assets/image/logo.png'

export default function Navbar() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light">
    <div className="container nav-style">
        <a className="navbar-brand">
            <img src= {logo} alt="logo" />
        </a>
        <a className="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" 
        aria-controls="offcanvasRight" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </a>
               <div className="offcanvas offcanvas-end" tabIndex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
                  <div className="offcanvas-header">
                        <h5 id="offcanvasRightLabel">BCR</h5>
                        <button type="button" className="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                  </div>
                      <div className="offcanvas-body">
                           <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                           <li>
                                <a class="nav-link text-dark" href="#">Our Service</a>
                            </li>
                            <li>
                                <a class="nav-link text-dark" href="#">Why Us</a>
                            </li>
                            <li>
                                <a class="nav-link text-dark" href="#">Testimonial</a>
                            </li>
                            <li>
                                <a class="nav-link text-dark" href="#">FAQ</a>
                            </li>
                        <div className="button-signup">
                            <button type="button" class="btn btn-success">Register</button>
                        </div>
                        </ul>
                    </div>
            </div>
    </div>
</nav>
  )
}
