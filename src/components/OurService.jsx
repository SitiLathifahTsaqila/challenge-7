import React from 'react'
import img_service from '../assets/image/img_service.png'
import {Container, Row, Col} from "react-bootstrap";
import centang from '../assets/image/centang.png'

export default function OurService() {
  return (
    <Container className="ms-auto">
    <div className="OurService" id="toOurService">
        <Row>
            <Col lg = {6} id="gambarService">
                <img src= {img_service} className="img-fluid" />
            </Col>
            <Col lg={6} className="align-self-center">
                <h2>Best Car Rental for any kind of trip in (Lokasimu)!</h2>
                <p>Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih 
                    murah dibandingkan yang lain, kondisi mobil baru, serta kualitas 
                    pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</p>
                <table>
                    <tbody>
                        <tr>
                            <td><img src= {centang} /></td>
                            <td className="checklist">Sewa Mobil Dengan Supir di (Lokasimu) 12 Jam</td>
                        </tr>
                        <tr>
                            <td><img src= {centang} /></td>
                            <td className="checklist">Sewa Mobil Lepas Kunci di (Lokasimu) 24 Jam</td>
                        </tr>
                        <tr>
                            <td><img src= {centang} /></td>
                            <td className="checklist">Sewa Mobil Jangka Panjang Bulanan</td>
                        </tr>
                        <tr>
                            <td><img src= {centang} /></td>
                            <td className="checklist">Gratis Antar - Jemput Mobil di Bandara</td>
                        </tr>
                        <tr>
                            <td><img src= {centang} /></td>
                            <td className="checklist">Layanan Airport Transfer / Drop In Out</td>
                        </tr>
                    </tbody>
                </table>
            </Col>
        </Row>	
    </div>
</Container>
  );
}
