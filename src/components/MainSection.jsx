import React from 'react'
import { Link } from "react-router-dom";
import {Row, Col, Button} from 'react-bootstrap';
import car from '../assets/image/car.png';

export default function MainSection() {
  return (
    <Row id="MainSection" >
    <Col lg= {6} className = "align-self-center">
        <div className="text" id="MainSection-text">
            <h1>Sewa & Rental Mobil Terbaik di Kawasan (Lokasimu) </h1>
            <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas 
            terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk 
            sewa mobil selama 24 jam.</p>
            <Link to="/cars"><Button className="btn-success" href="search_car.html">Mulai sewa mobil</Button></Link>
        </div>
    </Col>
    <Col lg= {6} id="mobil">
        <img src={car} className="img-fluid" />	
    </Col>
</Row>
  )
}
